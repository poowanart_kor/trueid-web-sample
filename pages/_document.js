import Document, { Head, Main, NextScript } from 'next/document'
export default class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }
  render() {
    return (
      <html>
        <Head>
          <meta charSet="utf-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        </Head>
        <body>
          <script src="https://accounts.trueid-dev.net/assets/trueid/js/trueid-sso-widget-react.js?ver=1.55"></script>
          <Main />
          <NextScript />
          <div id="trueid-login-widget" data-type="dark-bar-button" data-auto="N" data-style="modal" data-width="400" data-height="350" data-button="custom"></div>
        </body>
      </html>
    )
  }
}