import TrueID_AAA from 'widget-trueid-aaa'
import React, { Component } from 'react'

const AAAConfiguration = {
  'authUrl': 'https://accounts.trueid-dev.net/',
  'clientID': "29",
  'redirect_uri': "http://localhost:3000",
  'localhost': "http://localhost:3000",
  'lang': "en"
}

class Index extends Component {
  constructor(props) {
    super(props);
    TrueID_AAA.init(AAAConfiguration);
    this.state = { status: TrueID_AAA.getState().LOADING };
    this.handleLoad = this.handleLoad.bind(this);
    console.log(this.state);
  }

  componentDidMount() {//handle DOM is loaded
    window.addEventListener('load', this.handleLoad);
  }

  handleLoad() {//handle DOM is loaded
    this.handleGetProfile();
  }

  handleVerifyProductOnClick(e) {
    e.preventDefault()
    let waitPromise = TrueID_AAA.verifyProduct();
    waitPromise.then((response) => {
      //handle verifyProduct
    });
  }

  handleLoginOnClick(e) {
    e.preventDefault()
    let waitPromise = TrueID_AAA.login();
    waitPromise.then((response) => {
      if ('access_token' in response) {
        this.setState({ status: TrueID_AAA.getState().LOGGED });
        this.handleGetProfile();//after login then get profile
        console.log(this.state);
      }
    });
  }

  handleRegisterOnClick(e) {
    e.preventDefault()
    let waitPromise = TrueID_AAA.register();
    waitPromise.then((response) => {
      if ('access_token' in response) {
        this.setState({ status: TrueID_AAA.getState().LOGGED });
        console.log(this.state);
      }
    });
  }

  handleGetProfile() {
    let waitPromise = TrueID_AAA.getProfile();
    waitPromise.then((response) => {
      if ('uid' in response) {
        var responseJson = JSON.stringify(response);
        this.setState({ status: TrueID_AAA.getState().LOGGED });
        this.setState({ profile: responseJson });
        console.log(this.state);
      } else {//Retry just one time
        let waitPromise = TrueID_AAA.getProfile();
        waitPromise.then((response) => {
          if ('uid' in response) {
            var responseJson = JSON.stringify(response);
            this.setState({ status: TrueID_AAA.getState().LOGGED });
            this.setState({ profile: responseJson });
            console.log(this.state);
          } else {
            let waitPromise = TrueID_AAA.getProfile();
            this.setState({ status: TrueID_AAA.getState().GUEST });
          }
        });
      }

    });
  }
  render() {
    return (
      <div>
        <h1>Welcome to Next.js!</h1>
        {
          (this.state.status === TrueID_AAA.getState().LOADING) ? (
            <div>LOADING</div>
          ) : (this.state.status === TrueID_AAA.getState().GUEST) ? (
            <div>
              <div style={{ marginTop: 50, height: 300 }}><center><button style={{ backgroundColor: '#e02528', borderColor: 'transparent' }} id="loginbox" type="button" className="btn btn-primary btn-lg" onClick={e => this.handleLoginOnClick(e)}>Login</button><br /><br /><button style={{ backgroundColor: 'rgba(74,74,74,.5)', borderColor: 'transparent' }} id="register" type="button" className="btn btn-primary btn-lg" onClick={e => this.handleRegisterOnClick(e)}>Register</button></center></div>
            </div>
          ) : (
                <div>
                  <div style={{ marginTop: 100 }}><center><b>Get Profile Response : </b>{this.state.profile}</center></div>
                  <div style={{ marginTop: 30 }}><center><button style={{ backgroundColor: '#e02528', borderColor: 'transparent' }} id="loginbox" type="button" className="btn btn-primary btn-lg" onClick={e => this.handleVerifyProductOnClick(e)}>Mapping</button></center></div>
                </div>
              )
        }
      </div>
    )
  }
}

export default Index;
